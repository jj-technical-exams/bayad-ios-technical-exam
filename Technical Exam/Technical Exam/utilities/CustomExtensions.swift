//
//  CustomExtensions.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import UIKit


extension UIPageViewController {
    var isPagingEnabled: Bool {
        get {
            var isEnabled: Bool = true
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    isEnabled = subView.isScrollEnabled
                }
            }
            return isEnabled
        }
        set {
            for view in view.subviews {
                if let subView = view as? UIScrollView {
                    subView.isScrollEnabled = newValue
                }
            }
        }
    }
}


extension UITableView{
    
    public func setProperties() {
        self.allowsSelection = false
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = 120
        self.tableFooterView = UIView()
//        self.separatorStyle = .none
    }
    
}
