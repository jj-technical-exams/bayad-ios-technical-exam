//
//  CustomDesigns.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import UIKit


class CUIButton_Primary: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor(named: "BaseColor")
        self.layer.cornerRadius = CGFloat(Constant.DEFAULT_RADIUS)
    }

}


@IBDesignable
class CircularView: UIView {

    @IBInspectable var borderColor: UIColor = UIColor.red;
    @IBInspectable var borderSize: CGFloat = 4

    override func draw(_ rect: CGRect){
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderSize
        layer.cornerRadius = self.frame.height/2
    }
}
