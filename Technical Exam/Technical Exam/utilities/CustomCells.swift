//
//  CustomCells.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import UIKit

class ItemTableCell: UITableViewCell {
    
    @IBOutlet var view_read: UIView!
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_details: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
}

