//
//  ErrorMessage.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation


public class ErrorMessage {
    
    public func setErrorMessage(_ value : String?) -> String {
        
        let data = (nil != value ? value : "");
        
        var message :String = "";
        
        if (data!.contains("unexpected end of stream") || data!.contains("404") || data!.contains("500")) {
            message = "Cannot connect to the server.";
        
        }else if (data!.contains("Failed to connect to") || data!.contains("failed to connect to") || data!.contains("no response to server")){
            message = "Cannot connect to server.";
        
        }else if (data!.contains("Unable to resolve host")){
            message = "Cannot connect to server, please check your internet connection";
        
        }else if (data!.contains("timeout") || data!.contains("time out") || data!.contains("SocketTimeoutException")){
            message = "Connection Time Out";
        
        }else if (data!.contains("javax.net.ssl.SSLHandshakeException") || data!.contains("Handshake failed")){
            message = "Cannot construct secured connection";
        
        }else{
            message = "Something went wrong, Please try again.";
        }
        return message;
        
        
    }
    
}
