//
//  Constant.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation


class Constant{
    
    public static let DEFAULT_RADIUS = 3.0
    
    
    public static let TITLE_HOLD_ON = "Hold On!"
    public static let TITLE_NO_INTERNET = "Please check your internet connection."
    
    public static let BUTTON_CLOSE = "Close"
}
