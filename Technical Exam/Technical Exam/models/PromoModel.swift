//
//  PromoModel.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation


struct PromoModel : Codable {
    var _id : String
    var name : String
    var details : String
    var image_url : String
    var read : Int
    
    init( name: String,  details: String, read: Int) {
        self._id = ""
        self.name = name
        self.details = details
        self.image_url = ""
        self.read = read
    }
    
    enum CodingKeys: String, CodingKey {
        case _id
        case name
        case details
        case image_url
        case read
    }

}
