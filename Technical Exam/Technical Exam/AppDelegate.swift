//
//  AppDelegate.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        //enabling translucent navigation top bar
        let navAppearance = UINavigationBar.appearance()
        
        navAppearance.setBackgroundImage(UIImage(),for: .default)
        navAppearance.shadowImage = UIImage()
        navAppearance.isTranslucent = true
        
        navAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor(named: "BaseColor")!]
        
        //override default back button
        navAppearance.backIndicatorImage = UIImage(systemName: "arrow.backward")
        navAppearance.backIndicatorTransitionMaskImage = UIImage(systemName: "arrow.backward")
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -500.0, vertical: 0.0), for: .default)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }


}

