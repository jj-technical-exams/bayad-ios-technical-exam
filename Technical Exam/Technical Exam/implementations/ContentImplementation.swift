//
//  ContentImplementation.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import Alamofire


class ContentImplementation {
    var delegate : ContentDelegate?
    
    let errorMessage = ErrorMessage()
    
    let crudcrudBaseUrl = "https://crudcrud.com/api/"
    let crudcrudKey = "8d79ec59f1094f57bdea7330085789c3"
    
    let bayadBaseUrl = "/bayad/"
    
    
    //get promotions
    func getPromos(){
        
        AF.request(crudcrudBaseUrl + crudcrudKey + bayadBaseUrl,method: .get)
            .responseData(completionHandler: { (response) in
                switch response.result{
                case .success(let data):
                    do{
                        //attempt to convert into object
                        let body = (try JSONDecoder().decode([PromoModel].self, from : data))
                        
                        self.delegate!.onGetPromosSuccess(body)
                        
                    }catch let error as NSError {
                        debugPrint(error)
                        self.delegate!.onGetPromosError(self.errorMessage.setErrorMessage(error as? String)) // conditional downcast
                    }
                    
                case .failure(let error as NSError):
                    debugPrint(error)
                    self.delegate!.onGetPromosError(self.errorMessage.setErrorMessage(error as? String))  // conditional downcast
                }
            })
        
    }
    
    
    //tag as promo as read
    func readPromo(_ promo: PromoModel){
        
        AF.request((crudcrudBaseUrl + crudcrudKey + bayadBaseUrl + promo._id),
                   method: .put,
                   parameters: promo,
                   encoder: JSONParameterEncoder.default)
            .responseData(completionHandler: { (response) in
                switch response.result{
                case .success(_):
                    //tag as read the correspoding promo item only if api call is successful
                    self.delegate!.onReadPromosSuccess(promo._id)
                case .failure(let error as NSError):
                    debugPrint(error)
                //no need to alert the user about the error since tagging is done on background side
                }
            })
        
    }
}

protocol ContentDelegate {
    
    func onGetPromosSuccess(_ data:[PromoModel])
    func onGetPromosError(_ message:String)
    
    func onReadPromosSuccess(_ promoId: String)
}
