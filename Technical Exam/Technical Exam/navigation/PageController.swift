//
//  PageController.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import UIKit

class PageController: UIPageViewController{
    
    let _pageTitles = ["Updates","Promos","Transactions"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notification Inbox"
        
        
        self.dataSource = self
        
        pageViewSetIndex(1)
    }
}

extension PageController : UIPageViewControllerDataSource {
    
    func pageViewSetIndex(_ index: Int){
        self.setViewControllers([getViewControllerAtIndex(index)] as [UIViewController],
                                direction: UIPageViewController.NavigationDirection.forward,
                                animated: true,
                                completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent: PageContentController = viewController as! PageContentController
        
        var index = pageContent.viewIndex
        
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        
        index! -= 1;
        
        return getViewControllerAtIndex(index!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent: PageContentController = viewController as! PageContentController
        
        var index = pageContent.viewIndex
        
        if (index == NSNotFound){
            return nil;
        }
        
        index! += 1;
        if (index == _pageTitles.count){
            return nil;
        }
        return getViewControllerAtIndex(index!)
    }
    
    
    func getViewControllerAtIndex(_ index: Int) -> PageContentController{
        let content = self.storyboard?.instantiateViewController(withIdentifier: "PageContentController") as! PageContentController
        content.viewIndex = index

//        content.lbl_index.text = views[index]
        return content
    }
    
}
