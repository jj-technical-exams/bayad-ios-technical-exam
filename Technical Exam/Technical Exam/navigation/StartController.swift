//
//  ViewController.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import UIKit

class StartController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func navigateToPageController(_ sender: AnyObject) {
        let goToPage = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! PageController
        self.navigationController?.pushViewController(goToPage, animated: true)
    }
}

