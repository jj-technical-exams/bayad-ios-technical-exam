//
//  DisplayController.swift
//  Technical Exam
//
//  Created by John Joseph P. Magculang on 7/20/21.
//

import Foundation
import UIKit
import Reachability


class PageContentController : UIViewController{
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lbl_noPromos: UILabel!
    var refresher = UIRefreshControl()
    let implement = ContentImplementation()
    
    var viewIndex: Int!
    
    var arrayList: [PromoModel] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //temporary data
//        arrayList.append(PromoModel(name: "Bills mo, sagot ko promo", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 1))
//        arrayList.append(PromoModel(name: "All you need under 60 minutes!", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 1))
//        arrayList.append(PromoModel(name: "Rebates", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 0))
//        arrayList.append(PromoModel(name: "Save 10% Discount!", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 0))
//        arrayList.append(PromoModel(name: "Bayad Year-end promo is here!", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 0))
//        arrayList.append(PromoModel(name: "Bills mo, sagot ko promo", details: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla lectus sem, bibendum vitae rhoncus vel, pulvinar et mauris.", read: 1))
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.implement.delegate = self
        
        
        //init table
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.setProperties()
        
        self.refresher.tintColor = UIColor(named: "BaseColor")
        self.refresher.addTarget(self, action: #selector(onRefresh(_:)), for: .valueChanged)
        self.tableView.addSubview(self.refresher)
        
    }
    
    @objc func onRefresh(_ sender: UIRefreshControl){
        self.getPromos()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //initial service call
        self.refresher.beginRefreshing()
        self.tableView.setContentOffset(CGPoint(x: 0, y: self.tableView.contentOffset.y - self.refresher.frame.size.height), animated: true)
        self.getPromos()
        
        //hidden on initial viewing load
        self.lbl_noPromos.isHidden = true
    }
    
    
    func getPromos(){
        //detect any internet type
        if Reachability.Connection.self != .none{
            self.implement.getPromos()
        }else {
            let alert = UIAlertController(title: Constant.TITLE_HOLD_ON, message: Constant.TITLE_NO_INTERNET, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: Constant.BUTTON_CLOSE, style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func showAlert(_ message:String){
        let alert = UIAlertController(title: Constant.TITLE_HOLD_ON, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constant.BUTTON_CLOSE, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension PageContentController: ContentDelegate{
    func onGetPromosSuccess(_ data: [PromoModel]) {
        if refresher.isRefreshing {
            refresher.endRefreshing()
        }
        
        arrayList = []
        arrayList.append(contentsOf: data)
        
        
        //reload table view
        tableView.reloadData()
        lbl_noPromos.isHidden = !arrayList.isEmpty
    }
    
    func onGetPromosError(_ message: String) {
        if refresher.isRefreshing {
            refresher.endRefreshing()
        }
        
        self.showAlert(message)
    }
    
    //tag as read the local copy of the promo
    func onReadPromosSuccess(_ promoId: String) {
        for (x,i) in arrayList.enumerated(){
            if i._id == promoId {
                arrayList[x].read = 1
                
                //section is default to 0 since there is only 1 table cell
                tableView.reloadRows(at: [IndexPath(row: x, section: 0)], with: .automatic)
                break
            }
        }
        
    }
}
 


extension PageContentController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayList.count
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let promo = arrayList[indexPath.row]
        
        //get xib layout
        let identifier = "ItemTableCell"

        var cell: ItemTableCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? ItemTableCell
        if cell == nil {
            tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ItemTableCell
        }

        cell.backgroundColor = promo.read == 0 ? UIColor(named: "ItemUnreadColor") : UIColor.clear
        
        cell.view_read.isHidden = promo.read == 1
        
        cell.lbl_name.text = promo.name
        cell.lbl_details.text = promo.details
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promo = arrayList[indexPath.row]
        
        //MARK: todo navigate to details page
        let goTo = self.storyboard!.instantiateViewController(withIdentifier: "DetailController") as! DetailController
        goTo.promoModel = promo
        navigationController?.pushViewController(goTo, animated: true)
        
        
        //call api to tag promo as read
        implement.readPromo(promo)
    }
    
    
    
}
